milkytracker (1.05.01+dfsg-1) unstable; urgency=medium

  * New upstream version.

  * d/copyright:
    - Minor updates for 1.05.
    - Exclude resources/reference due to some files having unclear and
      possibly non-free licenses.
  * d/install: Install upstream AppStream metadata.
  * d/patches: Drop c++11.patch - alternative applied upstream.

 -- James Cowgill <jcowgill@debian.org>  Thu, 28 Nov 2024 22:50:08 +0000

milkytracker (1.04.00.20230830+dfsg-3) unstable; urgency=medium

  [ Christian Buschau ]
  * Use upstream desktop file.

 -- James Cowgill <jcowgill@debian.org>  Sat, 10 Aug 2024 12:15:37 +0100

milkytracker (1.04.00.20230830+dfsg-2) unstable; urgency=medium

  * Bump standards version to 4.7.0.

 -- Alex Myczko <tar@debian.org>  Tue, 16 Jul 2024 14:38:28 +0000

milkytracker (1.04.00.20230830+dfsg-1) unstable; urgency=medium

  * New upstream version of 1.04.00.
    - Upstream moved the existing 1.04.00 tag to another commit around the
      end of August.
    - Fixes crash on startup. (Closes: #1051256, LP: #2040293)

  * d/copyright: Update date.
  * d/patches:
    - Drop patches applied upstream.
    - Refresh 01_remove-resources-music.patch.
  * d/source/lintian-overrides: Override source-is-missing.

 -- James Cowgill <jcowgill@debian.org>  Thu, 30 Nov 2023 17:24:39 +0000

milkytracker (1.04.00+dfsg-1) unstable; urgency=medium

  * New upstream version. (Closes: #1016578)
    CVE-2022-34927 - stack overflow via the component LoaderXM::load
  * Bump standards version to 4.6.2.
  * d/control: update maintainer address.

 -- Gürkan Myczko <tar@debian.org>  Sun, 30 Jul 2023 13:44:04 +0200

milkytracker (1.03.00+dfsg-2) unstable; urgency=medium

  * Team upload.

  * Add patch to build with C++11 (Closes: #1000855)
  * Refresh patches (and add descriptions)
  * Apply 'wrap-and-sort -ast'
  * Add salsa-ci configuration
  * Add myself to copyright holders...
  * Bump standards version to 4.6.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Tue, 30 Nov 2021 17:29:52 +0100

milkytracker (1.03.00+dfsg-1) unstable; urgency=medium

  * New upstream version.
  * d/upstream/metadata: added.
  * d/copyright:
    - added Upstream-Contact.
    - update copyright years.
    - drop wildcard-matches for files not shipped anymore.
  * Bump debhelper version to 13.
  * Bump standards version to 4.5.0.
  * d/patches/*: comment out upstreamed patches.
  * Patched away privacy-breach that is for very old IE.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 11 Dec 2020 07:10:08 +0100

milkytracker (1.02.00+dfsg-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add upstream fix for use-after-free in the PlayerGeneric
    destructor (CVE-2020-15569) (Closes: #964797)
  * debian/control: Update Homepage to the current one.

 -- Adrian Bunk <bunk@debian.org>  Mon, 27 Jul 2020 16:26:05 +0300

milkytracker (1.02.00+dfsg-2) unstable; urgency=high

  [ Utkarsh Gupta ]
  * Add patch to fix heap-based and stack-based buffer overflows.
    (Closes: #933964) (Fixes: CVE-2019-14464, CVE-2019-14496, CVE-2019-14497)

  [ Ondřej Nový ]
  * d/control: Unify Maintainer field to Debian Multimedia Maintainers.
  * Use debhelper-compat instead of debian/compat.

  [ Gürkan Myczko ]
  * Update list of tracker music applications in d/control and
    d/milkytracker.1.

  [ Olivier Humbert ]
  * Update d/control : http -> https + remove 1 useless empty line.
  * Update d/copyright : http->https + remove 1 empty line.
  * Update d/milkytracker.desktop: add a French comment.

 -- James Cowgill <jcowgill@debian.org>  Mon, 28 Oct 2019 17:28:45 +0000

milkytracker (1.02.00+dfsg-1) unstable; urgency=medium

  * New upstream version.

  * debian/patches:
    - Drop patches applied upstream.

 -- James Cowgill <jcowgill@debian.org>  Sun, 25 Feb 2018 10:15:54 +0000

milkytracker (1.01.00+dfsg-2) unstable; urgency=high

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Set Vcs-* to salsa.debian.org

  [ James Cowgill ]
  * debian/compat:
    - Use debhelper compat 11.
  * debian/control:
    - Set maintainer to debian-multimedia@l.d.o.
    - Set Rules-Requires-Root: no.
    - Bump standards to 4.1.3.
  * debian/patches:
    - Apply upstream patches to fix various buffer overflows.
      Thanks to Johannes Schultz (Closes: #890407)

 -- James Cowgill <jcowgill@debian.org>  Thu, 22 Feb 2018 23:47:13 +0000

milkytracker (1.01.00+dfsg-1) unstable; urgency=medium

  * New upstream version.

  * debian/copyright:
    - Move apple-permissive license to the bottom.
    - Update for 1.01.
  * debian/patches:
    - Refresh.
    - Add upstream patch to fix lhasa, zziplib detection.
  * debian/rules:
    - Install documentation into lowercase doc dir.

 -- James Cowgill <jcowgill@debian.org>  Mon, 31 Jul 2017 11:26:28 +0100

milkytracker (1.0.0+dfsg-2) unstable; urgency=medium

  * Upload to unstable.

  * debian/control:
    - Change Gürkan's surname to Myczko (at his request).
    - Bump standards to 4.0.0 (no changes required).

 -- James Cowgill <jcowgill@debian.org>  Sun, 18 Jun 2017 22:29:52 +0100

milkytracker (1.0.0+dfsg-1) experimental; urgency=medium

  * New upstream version.
    - Fixes FTBFS with GCC 7. (Closes: #853551)

  * debian/control:
    - Update new homepage.
    - Update various build dependencies for 1.0.0.
  * debian/compat:
    - Use debhelper compat 10.
  * debian/copyright:
    - Update copyright information for 1.0.0.
    - Exclude non-free music samples from source tarball.
  * debian/patches:
    - Drop all existing patches (obsolete in 1.0.0).
    - Add patch to prevent installing non-free music samples.

 -- James Cowgill <jcowgill@debian.org>  Thu, 13 Apr 2017 23:43:29 +0100

milkytracker (0.90.86+dfsg-2) unstable; urgency=medium

  * debian/patches/02_debundle-rtmidi.patch:
    - Update patch to detect rtmidi with pkg-config. (Closes: #828957)

 -- James Cowgill <jcowgill@debian.org>  Wed, 29 Jun 2016 17:39:37 +0100

milkytracker (0.90.86+dfsg-1) unstable; urgency=medium

  * New upstream version. (Closes: #777424)
    - Fixes bugs reported by Jindřich Makovička. (Closes: #628235)
  * Remove milkyplay. (Closes: #716566, #818742, LP: #1019517)
    - The code is Debian specific, unmaintained, and broken since 2012.

  * debian/control:
    - Set the Multimedia Team as the maintainer, and Gürkan as an uploader.
    - Add myself to the list of uploaders.
    - Bump standards to 3.9.8.
    - Add Vcs-* fields.
  * debian/copyright:
    - Rewrite in machine readable format.
  * debian/doc-base:
    - Register MilkyTracker manual with doc-base.
  * debian/install:
    - Use dh-exec to rename milkytracker icon.
    - Install icon into correct directory.
  * debian/menu:
    - Remove per TC decision. (See: #741573)
  * debian/patches:
    - Drop all original patches - applied upstream.
    - 01_add-alsalibs: Link against alsa to fix linker errors.
    - 02_debundle-rtmidi: Build against system rtmidi.
    - 03_debundle-zziplib: Build against system zziplib. The code which FTBFS
      using gcc-6 is no longer used after this patch. (Closes: #811897)
    - 04_use-lhasa: The shipped copy of lha is under a non-free license, so
      port the lha decompession code to use the free lhasa library.
  * debian/rules:
    - Switch to debhelper 9 and use dh.
    - Enable all hardening options.
    - Enable parallel build.
  * debian/watch:
    - Add a watch file.

 -- James Cowgill <jcowgill@debian.org>  Sat, 07 May 2016 13:27:37 +0100

milkytracker (0.90.85+dfsg-2.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "ftbfs with GCC-4.7": add missing include to
    debian/milkyplay/milkyplay.cpp. (Closes: #667277)
  * Fix "FTBFS: ./zzip_file.h:34:18: fatal error: zlib.h: No such file
    or directory": add build dependency on zlib1g-dev.
    (Closes: #669432)
  * Add build dependency on libasound2-dev [linux-any] (dropped by
    libsdl1.2-dev).

 -- gregor herrmann <gregoa@debian.org>  Tue, 08 May 2012 17:57:06 +0200

milkytracker (0.90.85+dfsg-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix "FTBFS": add patch ftbfs-gzip.patch from Arch Linux, found by Paul
    Wise. (Closes: #661906)

 -- gregor herrmann <gregoa@debian.org>  Sun, 18 Mar 2012 14:18:55 +0100

milkytracker (0.90.85+dfsg-2) unstable; urgency=low

  * Apply 64bit_freebsd_fix.patch from homepage. (Closes: #626627)

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Mon, 23 May 2011 12:03:06 +0200

milkytracker (0.90.85+dfsg-1) unstable; urgency=low

  * New upstream version.
  * Repackaged without platforms.
  * debian/copyright: Updated copyright details and homepage address.
  * debian/control: Updated homepage address.
  * Added libzzip-dev to debian/control build depends.
  * Bump standards version to 3.9.2.
  * Bump debhelper version to 8.
  * Added debian/source/format.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Wed, 04 May 2011 11:28:45 +0100

milkytracker (0.90.80+dfsg-2) unstable; urgency=low

  * Apply patch to fix libjack loading bug. (Closes: #498900)
    Thanks <khades.ru@gmail.com> for pointing me to it.
  * Bump standards version.
  * Bump debhelper version.
  * Update debian/copyright formatting.
  * debian/rules: drop dh_desktop call.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Tue, 13 Oct 2009 07:27:39 +0200

milkytracker (0.90.80+dfsg-1) unstable; urgency=low

  * Initial release. (Closes: #467430)
  * Repackaged without platforms.
  * Added desktop file.

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Tue, 26 Feb 2008 09:29:43 +0100
